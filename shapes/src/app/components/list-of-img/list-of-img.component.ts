import { Component, OnInit } from '@angular/core';
import { ImageServiceService } from 'src/app/services/image-service.service';
import { Router } from '@angular/router';
import { ImagesDTO } from 'src/app/classes/ImagesDTO';

@Component({
  selector: 'app-list-of-img',
  templateUrl: './list-of-img.component.html',
  styleUrls: ['./list-of-img.component.css']
})
export class ListOfImgComponent implements OnInit {

  constructor( private img: ImageServiceService, private Router: Router) { }

  ngOnInit() {
    console.log(this.img.listimage)
  }
  opencanvas(thisimg:ImagesDTO){
    debugger
    this.img.get_Geometries(thisimg.ImageId).subscribe(p=>{this.img.arrGeometriesDTO=p;alert(p[0].DrawingName);
      this.img.get_lines(thisimg.ImageId).subscribe(p=>{this.img.arrLinesDTO=p; 
      this.img.imageToShow=thisimg.Images;this.img.ImagesDTO=thisimg;
      this.Router.navigate(['canvas']);})})
     


  }
}
