import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfImgComponent } from './list-of-img.component';

describe('ListOfImgComponent', () => {
  let component: ListOfImgComponent;
  let fixture: ComponentFixture<ListOfImgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfImgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
