import { Component, OnInit } from '@angular/core';
import { UserDTO } from 'src/app/classes/UserDTO';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import 'sweetalert2/src/sweetalert2.scss'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import {ImageServiceService} from '../../services/image-service.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private ImageServiceService:ImageServiceService,private loginService:UserService, private Router: Router) { }

  ngOnInit() {
    document.getElementById('id01').style.display = 'block'
    this.loginService.userDto=new UserDTO()
  }
  doesNotExist() {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Something went wrong!',
      footer: '<a href>Why do I have this issue?</a>'
    })
  }
  continue() {

    this.loginService.getUser(this.loginService.userDto.userName, this.loginService.userDto.password).subscribe(
      data => {
        if (data.length == 0) {
          debugger
          this.doesNotExist();
          this.loginService.userDto = new UserDTO()
        }
        else {
          this.loginService.userDto.id = data[0].id;
          if (data[0].id) {
            this.loginService.userDto.exists = true
            data[0].exists = true

          }

          if (data[0].exists == false) {
            this.doesNotExist();
            this.loginService.userDto = new UserDTO()
          }

          else {
            this.Router.navigate(['Menu']);
            this.ImageServiceService.get_images().subscribe(p=>{this.ImageServiceService.listimage=p;alert(this.ImageServiceService.listimage[0].ImageName);
            });

          }
        }
      },
      err => { alert(err.message); });

  }

}
