

import { Component, AfterViewInit, ElementRef, ViewChild, Input } from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { switchMap, takeUntil, pairwise, map, buffer } from 'rxjs/operators'
import { point } from 'src/app/classes/point';
import { ImageServiceService } from 'src/app/services/image-service.service';
import { GeometriesDTO } from 'src/app/classes/GeometriesDTO';
import { UserService } from 'src/app/services/user.service';
import { ShapesService } from 'src/app/services/shapes.service';
import { LinesDTO } from 'src/app/classes/LinesDTO';
@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements AfterViewInit {
  private cx: CanvasRenderingContext2D;
  private cx1: CanvasRenderingContext2D;
  Line: boolean
  Circle: boolean
  Rectangle: boolean
  private polyline: Subject<point> = new Subject();

  @ViewChild('canvasImg', { static: false }) public x: ElementRef
  @ViewChild('canvasShapes', { static: false }) public xShapes: ElementRef

  @Input() public width = 400;
  @Input() public height = 400;
  GeometriesDTO: GeometriesDTO
  LinesDTO: LinesDTO
  ngAfterViewInit(): void {
    this.Line = false
    this.Circle = false
    this.Rectangle = false
    const canvasElShapes: HTMLCanvasElement = this.xShapes.nativeElement;
    this.cx1 = canvasElShapes.getContext('2d');
    this.cx1.lineWidth = 3;
    this.cx1.lineCap = 'round';

    this.cx1.strokeStyle = '#000';
    const canvasEl: HTMLCanvasElement = this.x.nativeElement;
    this.cx = canvasEl.getContext('2d');
    this.cx.lineWidth = 3;
    this.cx.lineCap = 'round';

    this.cx.strokeStyle = '#000';
    this.drawShapesFromServer();
    this.captureEvents(canvasEl);
  }
  drawShapesFromServer() {
    if (this.img.arrGeometriesDTO) {
      for (let index = 0; index < this.img.arrGeometriesDTO.length; index++) {
        let pointcenter: point = new point(this.img.arrGeometriesDTO[index].CenterPointX, this.img.arrGeometriesDTO[index].CenterPointY)
        if (this.img.arrGeometriesDTO[index].ShapesId == 1) {
          this.DrawingCircle(pointcenter, this.img.arrGeometriesDTO[index].WidthX, this.img.arrGeometriesDTO[index].WidthY)
        }
        else {
          this.RectangleDrawing(pointcenter, this.img.arrGeometriesDTO[index].WidthX, this.img.arrGeometriesDTO[index].WidthY)
        }
      }
    }
    if (this.img.arrLinesDTO) {
      for (let index = 0; index < this.img.arrLinesDTO.length; index++) {
        let start = {
          x:this.img.arrLinesDTO[index].xStart,
          y: this.img.arrLinesDTO[index].yStart
        }; let end = {
          x:this.img.arrLinesDTO[index].xEnd,
          y: this.img.arrLinesDTO[index].yEnd
        };
        this.LineDrawing(start,end)

      }

    }
  }
  captureEvents(canvasEl: HTMLCanvasElement) {
    let rect = canvasEl.getBoundingClientRect();

    let bufferBy = fromEvent(canvasEl, 'mouseup');
    this.polyline.pipe(
      buffer(bufferBy)).subscribe(
        val => this.Straighten(val, canvasEl)
      )
    // this will capture all mousedown events from the canvas element
    fromEvent(canvasEl, 'mousedown')
      .pipe(
        switchMap((e) => {

          // after a mouse down, we'll record all mouse moves
          return fromEvent(canvasEl, 'mousemove')
            .pipe(

              // we'll stop (and unsubscribe) once the user releases the mouse
              // this will trigger a 'mouseup' event    
              takeUntil(fromEvent(canvasEl, 'mouseup')),
              // we'll also stop (and unsubscribe) once the mouse leaves the canvas (mouseleave event)
              takeUntil(fromEvent(canvasEl, 'mouseleave')),
              // pairwise lets us get the previous value to draw a line from
              // the previous point to the current point    
              pairwise()
            )
        })
      )
      .subscribe((res: [MouseEvent, MouseEvent]) => {
        let rect = canvasEl.getBoundingClientRect();

        // previous and current position with the offset
        let prevPos = {
          x: res[0].clientX - rect.left,
          y: res[0].clientY - rect.top
        };

        let currentPos = {
          x: res[1].clientX - rect.left,
          y: res[1].clientY - rect.top
        };

        this.drawOnCanvas(prevPos, currentPos);
      });

  }

  private drawOnCanvas(

    prevPos: { x: number, y: number },
    currentPos: { x: number, y: number }
  ) {

    this.polyline.next(currentPos);

    // incase the context is not set
    if (!this.cx) { return; }

    // start our drawing path
    this.cx.beginPath();

    // we're drawing lines so we need a previous position
    if (prevPos) {
      // sets the start point
      this.cx.moveTo(prevPos.x, prevPos.y); // from

      // draws a line from the start pos until the current position
      this.cx.lineTo(currentPos.x, currentPos.y);

      // strokes the current path with the styles we set earlier
      this.cx.stroke();
    }
  }
  avarage(arr: Array<number>): number {
    let sum: number = 0

    arr.forEach(element => {
      sum = sum + element
    });
    return sum / arr.length
  }
  RectangleDrawing(centerPoint: point, length: number, width: number) {
    this.GeometriesDTO = new GeometriesDTO(0, this.img.ImagesDTO.ImageId, this.cx1.strokeStyle.toString(), "Rectangl", "", 0, centerPoint.x
      , centerPoint.y, length, width, 2, "yello")
    let startingPoint: point = new point(centerPoint.x - length, centerPoint.y - width)

    this.cx1.beginPath();

    this.cx1.rect(startingPoint.x, startingPoint.y, length * 2, width * 2);
    this.cx1.stroke();
    this.ShapesService.saveGeometry(this.GeometriesDTO).subscribe(s => { alert(s[0].DrawingId) });
  }
  DrawingCircle(centerPoint: point, radiusX: number, radiusY: number) {

    this.GeometriesDTO = new GeometriesDTO(0, this.img.ImagesDTO.ImageId
      , this.cx1.strokeStyle.toString(), "Circle", ""
      , 0, centerPoint.x, centerPoint.y, radiusX, radiusY, 1, "yello");

    this.cx1.beginPath();
    this.cx1.ellipse(centerPoint.x, centerPoint.y, radiusX, radiusY, 0, 0, 2 * Math.PI);
    this.cx1.stroke();
    this.cx.clearRect
    this.ShapesService.saveGeometry(this.GeometriesDTO).subscribe(s => { });

  }
  LineDrawing(pointstart: { x: number, y: number }, pointend: { x: number, y: number }) {
    this.LinesDTO = new LinesDTO(0, this.img.ImagesDTO.ImageId, this.cx1.strokeStyle.toString(), "Line", "", 0, pointstart.x, pointstart.y, pointend.x, pointend.y)
    this.cx1.beginPath()
    this.cx1.moveTo(pointstart.x, pointstart.y)
    this.cx1.lineTo(pointend.x, pointend.y)
    this.cx1.stroke()
    this.ShapesService.saveLine(this.LinesDTO).subscribe(s => { });

  }
  Straighten(arr: Array<point>, canvasEl: HTMLCanvasElement) {
    let rect = canvasEl.getBoundingClientRect();
    if (arr) {
      let arrX: Array<number> = new Array<number>()
      let arrY: Array<number> = new Array<number>()
      arr.forEach(element => {
        arrX.push(element.x)
        arrY.push(element.y)
      });
      let xCenter: number = this.avarage(arrX)
      let yCenter: number = this.avarage(arrY)
      let distanceX: Array<number> = new Array<number>()
      let distanceY: Array<number> = new Array<number>()
      arr.forEach(element => {
        distanceX.push(Math.abs(xCenter - element.x))
        distanceY.push(Math.abs(yCenter - element.y))
      });
      let radiusX: number = this.avarage(distanceX)
      let radiusY: number = this.avarage(distanceY)

      let centerPoint: point = new point(xCenter, yCenter)

      if (this.Line) {
        this.LineDrawing(arr[0], arr[arr.length - 1])
        this.Line = false
      }
      else if (this.Rectangle) {
        this.RectangleDrawing(centerPoint, radiusX, radiusY)
        this.Rectangle = false

      }
      else {
        this.DrawingCircle(centerPoint, radiusX, radiusY)
        this.Circle = false
      }
      this.cx.clearRect(0, 0, rect.width, rect.height)
    }
    else {
      alert("kkkk")
    }
  }
  constructor(private img: ImageServiceService, private UserService: UserService, private ShapesService: ShapesService) { }

  ngOnInit() {
  }
  AddTextBox() {
    debugger
    var elem = <HTMLInputElement>(document.createElement('input'));
    elem.type = "input"
    var elemcolor = <HTMLInputElement>(document.createElement('input'))
    elem.type = "color"

    document.body.appendChild(elem)
    document.body.appendChild(elemcolor)

  }
}






