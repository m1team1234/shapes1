import { Component, OnInit } from '@angular/core';
import { UserDTO } from 'src/app/classes/UserDTO';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
newUser:UserDTO
password1:string
checkpassowordboolean:boolean

  constructor(private loginService:UserService, private Router: Router) { }

  ngOnInit() {
    document.getElementById('id01').style.display = 'block'
    this.newUser=new UserDTO()
    this.password1=""
    this.checkpassowordboolean=false
  }
  checkpassoword(){
     if(!(this.password1===this.newUser.password))
     {
      this.checkpassowordboolean=!this.checkpassowordboolean
      //  this.password1=""
     }
     else{
      this.checkpassowordboolean=false
     }
  }
  continue(){
    
    this.loginService.addUser(this.newUser.userName,this.newUser.password).subscribe(
      data => {

        this.Router.navigate(['Menu']);
      },
      err => { alert(err.message); }
    )
  }

}
