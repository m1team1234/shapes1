import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
    ngOnInit(): void {
        document.getElementById("myNav").style.width = "25%";
    }
     openNav() {
        document.getElementById("myNav").style.width = "25%";
      }
      
       closeNav() {
        document.getElementById("myNav").style.width = "0%";
      }
 
  constructor() { }



}
