import { Component, OnInit } from '@angular/core';
import { d } from 'src/app/classes/d';
import { HttpClient } from '@angular/common/http';
import { ImageServiceService } from 'src/app/services/image-service.service';
import 'sweetalert2/src/sweetalert2.scss'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { ImagesDTO } from 'src/app/classes/ImagesDTO';
@Component({
  selector: 'app-uploud-file',
  templateUrl: './uploud-file.component.html',
  styleUrls: ['./uploud-file.component.css']
})
export class UploudFileComponent implements OnInit {
  global_image: any;
  constructor( private userService: UserService,private Http: HttpClient, private img: ImageServiceService,private router: Router) { }
  uploud: boolean
  ngOnInit() {
    this.img.ImagesDTO=new ImagesDTO()
    this.img.ImagesDTO.UserId=this.userService.userDto.id
    this.uploud = false
  }
  readURL(input) {
    this.global_image = input;
    let mybase64File = (input.target as any).result
    let fileNeme = input.target.files[0].name;
    sessionStorage.setItem("fileNeme", fileNeme)
    if (input.target.files && input.target.files[0]) {
      var reader = new FileReader()
      reader.onload = function (e) {
        debugger;
        let mybase64File = (e.target as any).result
        sessionStorage.setItem("mybase64File", mybase64File)
      }
      reader.readAsDataURL(input.target.files[0]);
    }
    this.saveFile();
  }
  saveFile() {

    let y: string = sessionStorage.getItem("mybase64File")

    let j = sessionStorage.getItem("fileNeme")
    let d1: d = new d();
    d1.base64 = y;
    this.Http.post<string>("https://localhost:44341/api/File/SaveFile/" + j + "/", d1).subscribe(p => {
      this.img.imageToShow = "https://localhost:44341/filesnew/" + p; this.img.ImagesDTO.ImageName=p;this.img.ImagesDTO.Images="https://localhost:44341/filesnew/" + p;
        this.img.saveimg(this.img.ImagesDTO).subscribe(p=> {this.img.ImagesDTO.ImageId=p[0].ImageId;
        this.img.listimage.push(p[0])});
      this.imagSave();

    }
    )
  }
  imagSave() {
    
    Swal.fire({
  icon: 'success',
  title: 'Image successfully uploaded'
  
}).then((result) => 
  {
    if (result.value) 
    {
      this.router.navigate(['canvas']);
    }
  })

  }

}