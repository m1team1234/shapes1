import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  imageToShow: string;
  ngOnInit(): void {
    this.imageToShow="../../../assets/img/crayons.png";
  }
  title = 'shapes';

}
