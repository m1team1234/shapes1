import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { CanvasComponent } from './components/canvas/canvas.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { UserService } from './services/user.service';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { appNavigations } from 'src/routingStructre';
import { RouterModule } from '@angular/router';
import { UploudFileComponent } from './components/uploud-file/uploud-file.component';
import { MenuComponent } from './components/menu/menu.component';
import { ListOfImgComponent } from './components/list-of-img/list-of-img.component';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    CanvasComponent,
    LoginComponent,
    UploudFileComponent,
    MenuComponent,
    ListOfImgComponent,
    
  ],
  imports: [
    RouterModule,
    BrowserModule,FormsModule,HttpClientModule,ReactiveFormsModule, RouterModule.forRoot(appNavigations, {enableTracing: true})
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }



