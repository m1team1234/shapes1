import { DrawingsDTO } from './DrawingsDTO'

export  class GeometriesDTO extends DrawingsDTO
{    constructor(
        public DrawingId?: number,
        public ImageId?: number,
        public LineColor?: string,
        public DrawingName?: string,
        public Comments?: string,
        public GeometryId?: number,
        public CenterPointX?: number,
        public CenterPointY?: number,
        public WidthX?: number,
        public WidthY?: number,
        public ShapesId?: number,
        public Color?: string,

        ) {
        super();
    }
}

