export class ImagesDTO {
    constructor(
        public ImageId?: number,
        public UserId?: number,
        public Images?: string,
        public ImageName?: string,
        public comments?: string,

        ) { }
}

