export abstract class DrawingsDTO
 {
    constructor(
        public DrawingId?: number,
        public ImageId?: number,
        public LineColor?: string,
        public DrawingName?: string,
        public Comments?: string,

        ) { }
}

