import { DrawingsDTO } from './DrawingsDTO';

export  class LinesDTO extends DrawingsDTO{
    /**
     *
     */
    constructor(
        public DrawingId?: number,
        public ImageId?: number,
        public LineColor?: string,
        public DrawingName?: string,
        public Comments?: string,
        public LineId?: number,
        public xStart?: number,
        public yStart?: number,
        public xEnd?: number,
        public yEnd?: number,
        
    ) {
        super();
        
    }
}
