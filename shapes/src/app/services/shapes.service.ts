import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {GeometriesDTO} from '../classes/GeometriesDTO'
import { Observable } from 'rxjs';
import { LinesDTO} from '../classes/LinesDTO';
@Injectable({
  providedIn: 'root'
})
export class ShapesService {
  basicUrl:string="https://localhost:44341/api/shepes/"

  constructor(private http: HttpClient) { }
  saveGeometry(GeometriesDTO:GeometriesDTO):Observable<Array<GeometriesDTO>> {
    return this.http.post<Array<GeometriesDTO>>(this.basicUrl + "addGeometry",[GeometriesDTO] );
  }
  saveLine(LinesDTO:LinesDTO):Observable<Array<LinesDTO>>{
        return this.http.post<Array<LinesDTO>>(this.basicUrl + "addLine",[LinesDTO]);

  }
}
