import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ImagesDTO } from '../classes/ImagesDTO'
import { UserService } from '../services/user.service'
import { LinesDTO } from '../classes/LinesDTO';
import { GeometriesDTO } from '../classes/GeometriesDTO';
@Injectable({
  providedIn: 'root'
})
export class ImageServiceService implements OnInit {
  basicUrl: string = "https://localhost:44341/api/File/"
  ngOnInit(): void {
    this.listimage = new Array<ImagesDTO>();
    this.arrGeometriesDTO=new Array<GeometriesDTO>();
    this.listimage=new Array<LinesDTO>();
  }
  imageToShow: any
  ImagesDTO: ImagesDTO
  arrGeometriesDTO:Array<GeometriesDTO>
  arrLinesDTO:Array<LinesDTO>
  listimage: Array<ImagesDTO>
  saveimg(ImagesDTO: ImagesDTO): Observable<Array<ImagesDTO>> {
    return this.http.put<Array<ImagesDTO>>(this.basicUrl + "savefileserver", [ImagesDTO]);
  }
  get_images(): Observable<Array<ImagesDTO>> {
    return this.http.post<Array<ImagesDTO>>(this.basicUrl + "getImages", [this.UserService.userDto.id])
  }
  get_lines(idimage:number):Observable<Array<LinesDTO>>{
    return this.http.post<Array<LinesDTO>>(this.basicUrl + "getline", [idimage]);
  } 
  get_Geometries(idimage:number):Observable<Array<GeometriesDTO>>{
    return this.http.post<Array<GeometriesDTO>>(this.basicUrl + "getgeometries", [idimage]);
  }

  constructor(private http: HttpClient, private UserService: UserService) {


  }
}


