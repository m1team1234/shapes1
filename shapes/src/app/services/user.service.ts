import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserDTO } from '../classes/UserDTO';

@Injectable({
  providedIn: 'root'
})
export class UserService implements OnInit {
  userDto: UserDTO

  ngOnInit() {
    this.userDto = new UserDTO()
  }

  constructor(private http: HttpClient) { }
  basicUrl: string = "https://localhost:44341/api/user/"

  getUser(userName: string, password: string): Observable<Array<UserDTO>> {
    debugger
    return this.http.put<Array<UserDTO>>(this.basicUrl + "findUser", [userName, password]);
  }


  addUser(userName: string, password: string): Observable<Array<UserDTO>> {
    return this.http.post<Array<UserDTO>>(this.basicUrl + "addUser", [userName, password]);
  }

  resetPasswordByuserName(userName: string): Observable<string> {
    return this.http.post<string>(this.basicUrl + "api/user/resetPassword", userName);
  }

  deleteUser(u: UserDTO): Observable<boolean> {
    return this.http.delete<boolean>(this.basicUrl + "Delete/" + u);
  }


  updateUser(u: UserDTO) {
    return this.http.put<UserDTO>(this.basicUrl + "api/UserDTO/P\putUserDTO", u);
  }
}
