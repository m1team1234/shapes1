import { CanvasComponent } from './app/components/canvas/canvas.component';
import { LoginComponent } from './app/components/login/login.component';
import { RegisterComponent } from './app/components/register/register.component';
import { UploudFileComponent } from './app/components/uploud-file/uploud-file.component';
import { ListOfImgComponent } from './app/components/list-of-img/list-of-img.component';
import { MenuComponent } from './app/components/menu/menu.component';

export const appNavigations = [

    {
        path: 'Menu', component: MenuComponent, children: [
             {path: 'canvas', component: CanvasComponent},
             {path: 'Upload', component: UploudFileComponent},
            {path: 'List', component: ListOfImgComponent},
        ]
    },
    {path: 'login', component: LoginComponent},

    {
        path: 'register', component: RegisterComponent
    },

   
  
    {
        path: '', redirectTo: "login", pathMatch: 'full'
    },

]
